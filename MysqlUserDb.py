import mysql.connector
from mysql.connector import Error
import settings as settings

class MysqlUserDb:

    def __init__(self):

        try:
            self.connection = mysql.connector.connect(**settings.DB_CONNECT)
            if self.connection.is_connected():
                db_Info = self.connection.get_server_info()
                print("Connected to MySQL Server version ", db_Info)
                self.cursor = self.connection.cursor()
                # self.cursor.execute("select database();")
                # record = self.cursor.fetchone()
                # print("You're connected to database: ", record)

        except Error as e:
            print("Error while connecting to MySQL", e)
        # finally:
        #   if (self.connection.is_connected()):
        #     self.cursor.close()
        #     self.connection.close()
        #     print("MySQL self.connection is closed")

    def insert_data(self, data):
        self.cursor.execute("""INSERT INTO bets (jour, reun, prix, cheval, num, rapportDirectInline, rapportDirectOffline, rapportRefInline, rapportRefOffline, indicateurTendance, nombreIndicateurTendance, dateRapport, favoris, grossePrise, enjeuInlineSG, totalEnjeuInline, updateTimeInline, enjeuOfflineSG, totalEnjeuOffline, updateTimeOffline, isDepartImminent, enjeuInlineSP, enjeuOfflineSP) VALUES(%(jour)s, %(reun)s, %(prix)s, %(cheval)s, %(num)s, %(rapportDirectInline)s, %(rapportDirectOffline)s, %(rapportRefInline)s, %(rapportRefOffline)s, %(indicateurTendance)s, %(nombreIndicateurTendance)s, %(dateRapport)s, %(favoris)s, %(grossePrise)s, %(enjeuInlineSG)s, %(totalEnjeuInline)s, %(updateTimeInline)s, %(enjeuOfflineSG)s, %(totalEnjeuOffline)s, %(updateTimeOffline)s, %(isDepartImminent)s, %(enjeuInlineSP)s, %(enjeuOfflineSP)s)""", data)

        # self.cursor.execute(sql, data)
        self.connection.commit()

