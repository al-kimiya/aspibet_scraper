import json
import http.client
import urllib.parse
import settings as settings


class ParseDb:
    def __init__(self):
        self.connection = http.client.HTTPConnection(settings.DB_MONGO_CONNECT)
        self.connection.connect()

    def insert_data(self, data):
        self.connection.request('POST', '/parse/classes/Bet', json.dumps(data), {
            "X-Parse-Application-Id": "APPLICATION_ID",
            "X-Parse-REST-API-Key": "REST_API_KEY",
            "Content-Type": "application/json"
            })
        results = json.loads(self.connection.getresponse().read())
        # print(results)

    def insert_data_formated(self, data):
        self.connection.request('POST', '/parse/classes/BetFormated', json.dumps(data), {
            "X-Parse-Application-Id": "APPLICATION_ID",
            "X-Parse-REST-API-Key": "REST_API_KEY",
            "Content-Type": "application/json"
            })
        results = json.loads(self.connection.getresponse().read())
        # print(results)

    def get_data_race(self, jour, reun, prix, limit):
        params = urllib.parse.urlencode({
            "where":json.dumps(
                {
                    "jour": jour,
                    "prix": prix,
                    "reun": reun
                }),
                "order":"-dateRapport",
                "limit":limit
        })
        self.connection.request('GET', '/parse/classes/Bet?%s' % params, '', {
            "X-Parse-Application-Id": "APPLICATION_ID",
            "X-Parse-REST-API-Key": "REST_API_KEY"
            })
        result = json.loads(self.connection.getresponse().read())
        
        return result
