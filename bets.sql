-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 07 Juillet 2020 à 17:52
-- Version du serveur :  5.7.30-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `aspibet`
--

-- --------------------------------------------------------

--
-- Structure de la table `bets`
--

CREATE TABLE `bets` (
  `id` int(11) NOT NULL,
  `jour` date NOT NULL,
  `reun` int(11) NOT NULL,
  `prix` int(11) NOT NULL,
  `cheval` varchar(255) NOT NULL,
  `num` tinyint(4) NOT NULL,
  `rapportDirectInline` decimal(5,2) NOT NULL,
  `rapportDirectOffline` decimal(5,2) NOT NULL,
  `rapportRefInline` decimal(5,2) NOT NULL,
  `rapportRefOffline` decimal(5,2) NOT NULL,
  `indicateurTendance` varchar(1) NOT NULL,
  `nombreIndicateurTendance` decimal(5,2) NOT NULL,
  `dateRapport` int(11) NOT NULL,
  `favoris` tinyint(1) NOT NULL,
  `grossePrise` tinyint(1) NOT NULL,
  `enjeuInlineSG` int(11) NOT NULL,
  `enjeuInlineSP` int(11) NOT NULL,
  `enjeuOfflineSG` int(11) NOT NULL,
  `enjeuOfflineSP` int(11) NOT NULL,
  `totalEnjeuInline` int(11) NOT NULL,
  `updateTimeInline` int(11) NOT NULL,
  `totalEnjeuOffline` int(11) NOT NULL,
  `updateTimeOffline` int(11) NOT NULL,
  `isDepartImminent` tinyint(1) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `bets`
--
ALTER TABLE `bets`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `bets`
--
ALTER TABLE `bets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;