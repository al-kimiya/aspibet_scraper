import sys
import requests
import datetime
import json
import io
import os, errno
from pathlib import Path
import MysqlUserDb as MySQL
import ParseDb as fun
from functools import reduce

# https://offline.turfinfo.api.pmu.fr/rest/client/7/programme/08032020/R1/C9/
# https://offline.turfinfo.api.pmu.fr/rest/client/7/programme/08032020/R1/C9/combinaisons?specialisation=INTERNET
# https://offline.turfinfo.api.pmu.fr/rest/client/7/programme/08032020/R1/C9/citations?paris=SIMPLE_GAGNANT&specialisation=OFFLINE&groupe=true
# https://online.turfinfo.api.pmu.fr/rest/client/1/programmes/08032020/R1/C9/simulateur/pari/E_SIMPLE_GAGNANT/rapports
# https://online.turfinfo.api.pmu.fr/rest/client/1/programmes/08032020/R1/C9/simulateur/pari/SIMPLE_GAGNANT/rapports

URL = 'https://online.turfinfo.api.pmu.fr/rest/client/7/programme/'

def main(argv):
    tagdate = datetime.datetime.now().date()
    try:
        sys.argv[1]
        print("sys", sys.argv[1])
        # tagdate = datetime.datetime.now().date()
        tagdate += datetime.timedelta(days=1)
        tagdate = str(tagdate)
        print("tagdate", tagdate)

    except:
        tagdate = str(tagdate)
        print("tagdate1", tagdate)

    print(tagdate)
    # tagdate = "2020-07-08"
    courses = search_prog(tagdate)
    print(courses)
    for reunion in courses:
        for course in courses[reunion]:
            print("Réunion:", reunion, "course:", course)
            read_json(tagdate, reunion, course)
    # read_json(tagdate, 5, 7)

def read_json(tagdate, reun, prix):
    data = {}
    datas = list()
    date = datetime.datetime.strptime(tagdate, '%Y-%m-%d')
    jour = date.strftime('%Y-%m-%d')
    datedmY = date.strftime('%d%m%Y')

    programme = requests.get(URL + datedmY+ '/R' + str(reun) + '/C' + str(prix))
    dataProgramme = json.loads(programme.text)
    try:
        data['isDepartImminent'] = dataProgramme['isDepartImminent']
    except:
        data['isDepartImminent'] = False


    if dataProgramme['statut'] == 'DEPART_CONFIRME' or dataProgramme['categorieStatut'] == 'ARRIVEE':
        print('Course partie ou terminée')
        return

    # Config mysql
    mysql = MySQL.MysqlUserDb()
    # Config mongodb
    mongo = fun.ParseDb()


    participantsInline = requests.get(URL + datedmY+ '/R' + str(reun) + '/C' +
        str(prix) + '/participants?specialisation=INTERNET')
    dataPmuInline = json.loads(participantsInline.text)

    participantsOffline = requests.get(URL + datedmY+ '/R' + str(reun) + '/C' +
        str(prix) + '/participants?specialisation=OFFLINE')
    try:
        dataPmuOffline = json.loads(participantsOffline.text)
    except:
        pass

    combiInlineSG = requests.get(URL + datedmY+ '/R' + str(reun) + '/C' + 
        str(prix) + '/citations?paris=E_SIMPLE_GAGNANT&specialisation=INTERNET')
    dataCombiInlineSG = json.loads(combiInlineSG.text)['listeCitations']
    
    combiOfflineSG = requests.get(URL + datedmY+ '/R' + str(reun) + '/C' +
        str(prix) + '/citations?paris=SIMPLE_GAGNANT&specialisation=OFFLINE')
    try:
        dataCombiOfflineSG = json.loads(combiOfflineSG.text)['listeCitations']
    except:
        pass

    combiInlineSP = requests.get(URL + datedmY+ '/R' + str(reun) + '/C' + 
        str(prix) + '/citations?paris=E_SIMPLE_PLACE&specialisation=INTERNET')
    dataCombiInlineSP = json.loads(combiInlineSP.text)['listeCitations']
    
    combiOfflineSP = requests.get(URL + datedmY+ '/R' + str(reun) + '/C' +
        str(prix) + '/citations?paris=SIMPLE_PLACE&specialisation=OFFLINE')
    try:
        dataCombiOfflineSP = json.loads(combiOfflineSP.text)['listeCitations']
    except:
        pass

    simulateurInline = requests.get(URL + datedmY+ '/R' + str(reun) + '/C' + 
        str(prix) + '/simulateur/pari/E_SIMPLE_GAGNANT/rapports')
    try:
        dataSimulateurInline = json.loads(simulateurInline.text)
    except:
        pass

    simulateurOffline = requests.get(URL + datedmY+ '/R' + str(reun) + '/C' +
        str(prix) + '/simulateur/pari/SIMPLE_GAGNANT/rapports')
    try:
        dataSimulateurOffline = json.loads(simulateurOffline.text)
    except:
        pass


    data['jour'] = jour
    data['reun'] = reun
    data['prix'] = prix

    updateTimeInlineLast = None
    updateTimeOfflineLast = None

    getLastRace = mongo.get_data_race(jour, reun, prix, 1)
    try:
        updateTimeInlineLast = getLastRace['results'][0]['updateTimeInline']
        updateTimeOfflineLast = getLastRace['results'][0]['updateTimeOffline']
    except:
        pass

    try:
        data['totalEnjeuInline'] = dataSimulateurInline['totalEnjeu']
    except:
        data['totalEnjeuInline'] = 0
    try:
        data['totalEnjeuOffline'] = dataSimulateurOffline['totalEnjeu']
    except:
        data['totalEnjeuOffline'] = 0

    try:
        data['updateTimeInline'] = int(dataCombiInlineSG[0]['updatetime'] / 1000)
    except:
        data['updateTimeInline'] = 0
    try:
        data['updateTimeOffline'] = int(dataCombiOfflineSG[0]['updatetime'] / 1000)
    except:
        data['updateTimeOffline'] = 0

    if (data['updateTimeInline'] == updateTimeInlineLast and 
        data['updateTimeOffline'] == updateTimeOfflineLast):
        print('Date identique avec le dernier enregistrement')
        return

    for items in dataPmuInline['participants']:
        data['cheval'] = items['nom']
        data['num'] = items['numPmu']
        try:
            data['dateRapport'] = int(items['dernierRapportDirect']['dateRapport'] / 1000)
            data['indicateurTendance'] = items['dernierRapportDirect']['indicateurTendance']
            data['nombreIndicateurTendance'] = items['dernierRapportDirect']['nombreIndicateurTendance']
            data['favoris'] = items['dernierRapportDirect']['favoris']
            data['grossePrise'] = items['dernierRapportDirect']['grossePrise']
        except:
            print("Information is not yet available in the file ", URL + datedmY+ '/R' + str(reun) + '/C' +
        str(prix) + '/participants?specialisation=INTERNET')
            continue

        try:
            data['rapportDirectInline'] = items['dernierRapportDirect']['rapport']
        except:
            data['rapportDirectInline'] = 0
        try:
            data['rapportRefInline'] = dataSimulateurInline['rapportsParticipant'][items['numPmu'] - 1]['rapportReference']
        except:
            data['rapportRefInline'] = 0
        try:
            data['enjeuInlineSG'] = dataCombiInlineSG[0]['participants'][items['numPmu'] - 1]['citations'][0]['enjeu']
        except:
            data['enjeuInlineSG'] = 0
        try:
            data['enjeuInlineSP'] = dataCombiInlineSP[0]['participants'][items['numPmu'] - 1]['citations'][0]['enjeu']
        except:
            data['enjeuInlineSP'] = 0

        try:
            data['rapportDirectOffline'] = dataPmuOffline['participants'][items['numPmu'] - 1]['dernierRapportDirect']['rapport']
        except:
            data['rapportDirectOffline'] = 0
        try:
            data['rapportRefOffline'] = dataSimulateurOffline['rapportsParticipant'][items['numPmu'] - 1]['rapportReference']
        except:
            data['rapportRefOffline'] = 0
        try:
            data['enjeuOfflineSG'] = dataCombiOfflineSG[0]['participants'][items['numPmu'] - 1]['citations'][0]['enjeu']
        except:
            data['enjeuOfflineSG'] = 0
        try:
            data['enjeuOfflineSP'] = dataCombiOfflineSP[0]['participants'][items['numPmu'] - 1]['citations'][0]['enjeu']
        except:
            data['enjeuOfflineSP'] = 0
        
        # Save mongodb
        mongo.insert_data(data)

        # Save mysql
        mysql.insert_data(data)
        datas.append(data.copy())

    print("data length", len(datas))
    if len(datas) != 0:
        format_datas(datas)

def format_datas(datas):
    mongo = fun.ParseDb()
    data = {}
    rapportDirectInline = {}
    rapportDirectOffline = {}
    enjeuInlineSG = {}
    enjeuInlineSP = {}
    enjeuOfflineSG = {}
    enjeuOfflineSP = {}
    totalEnjeuCheval = {}
    coteCorrige = {}
    data['jour'] = datas[0]['jour']
    data['reun'] = str(datas[0]['reun'])
    data['prix'] = str(datas[0]['prix'])

    totalEnjeuCourseMap = list(map(
        lambda x: 
            x['enjeuInlineSG'] + x['enjeuInlineSP'] + x['enjeuOfflineSG'] + x['enjeuOfflineSP'],
            datas
    ))
    totalEnjeuCourse = sum(totalEnjeuCourseMap) / 100
    print('totalEnjeuCourse', totalEnjeuCourse)

    for race in datas: 
        rapportDirectInline[race['num']] = race['rapportDirectInline']
        rapportDirectOffline[race['num']] = race['rapportDirectOffline']
        enjeuInlineSG[race['num']] = race['enjeuInlineSG'] / 100
        enjeuInlineSP[race['num']] = race['enjeuInlineSP'] / 100
        enjeuOfflineSG[race['num']] = race['enjeuOfflineSG'] / 100
        enjeuOfflineSP[race['num']] = race['enjeuOfflineSP'] / 100
        totalEnjeuChevalSum = (race['enjeuInlineSG'] + race['enjeuInlineSP'] + \
            race['enjeuOfflineSG'] + race['enjeuOfflineSP']) / 100
        print('totalEnjeuChevalSum', totalEnjeuChevalSum)
        print('totalEnjeuChevalSum', totalEnjeuChevalSum)
        totalEnjeuCheval[race['num']] = totalEnjeuChevalSum
        try:
            coteCorrige[race['num']] = round(((totalEnjeuCourse - totalEnjeuChevalSum) / totalEnjeuChevalSum) + 1, 1)
        except:
            coteCorrige[race['num']] = 0

    data['rapportDirectInline'] = rapportDirectInline
    data['rapportDirectOffline'] = rapportDirectOffline
    data['enjeuInlineSG'] = enjeuInlineSG
    data['enjeuInlineSP'] = enjeuInlineSP
    data['enjeuOfflineSG'] = enjeuOfflineSG
    data['enjeuOfflineSP'] = enjeuOfflineSP
    data['coteCorrige'] = coteCorrige
    data['totalEnjeuCheval1'] = totalEnjeuCheval
    mongo.insert_data_formated(data)


def search_prog(tagdate):
    """
    Create a dict to get the meeting numbers
    and the race numbers because the numbers are not necessarily in order
    {1: [1, 2,...], 2: [1, 2...], 3: [1, 2...], 4: [7, 8, 9]}
    """
    date = datetime.datetime.strptime(tagdate, '%Y-%m-%d')
    datedmY = date.strftime('%d%m%Y')

    programme = requests.get(URL + datedmY + '?specialisation=INTERNET')
    dataProgramme = json.loads(programme.text)
    reunions = dataProgramme['programme']['reunions']
    courses = {}
    for nb in range(len(reunions)):
        numReunion = reunions[nb]['numOfficiel']
        dataCourses = []
        for i in reunions[nb]['courses']:
            dataCourses.append(i['numOrdre'])
        courses[numReunion] = dataCourses
    return courses


main(sys.argv)



# categorieStatut	"ARRIVEE"

# statut	"DEPART_CONFIRME"
# categorieStatut	"EN_COURS"

# statut	"PROGRAMMEE"
# categorieStatut	"A_PARTIR"


