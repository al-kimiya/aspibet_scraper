# Aspibet

Enregistre les cotes et les enjeux pour chaque cheval et chaque course du PMU afin de faire un suivi de l'évolution dans la journée.

## Pré-requis

 * Node.js
 * MongoDB
 * [Parse Server]
   * Utilisé pour enregistrer dans la base MongoDB
   * Voir la Configuration [ci-dessous](#Parse-Server)
 * Mysql (non obligatoire mais une copie des données peut être enregistrée dans une base Mysql)


## Configuration

* Créer la base de données en utilisant le fichier ```bets.sql``` si vous souhaitez utiliser une base Mysql

Si vous ne souhaitez pas utiliser Mysql, il faut commenter dans le fichier ```main.py``` les lignes
```py
import MysqlUserDb as MySQL
...
mysql = MySQL.MysqlUserDb()
...
mysql.insert_data(data)

```

* Renommer le fichier ```settings-env.py``` en ```settings.py```
* Configurer l'accès à la base de données Mysql dans ```settings.py```

```sh
DB_CONNECT = {
  'db': 'database',
  'user': 'user',
  'passwd': 'password',
  'host': 'localhost',
  'charset': 'utf8',
  'use_unicode': True,
}
```

* Configurer l'adresse pour Parse Server dans ```settings.py```, port 1337 par défaut
```
DB_MONGO_CONNECT='localhost:1337'

```

## Parse Server

Parse Server est un backend qui permet d'enregistrer dans la base MongoDB et permet d'avoir la synchronisation des données en temps réel pour mettre à jour votre frontend par exemple.

 * Installer [Parse Server]
 * Dans le dossier, créer ou modifier le fichier ```index.js```

voici un exemple de configuration avec MongoDB qui tourne sur le port ```27017``` et une base qui se nomme ```test```

```js
// https://github.com/parse-community/parse-server

const express = require('express');
const { default: ParseServer, ParseGraphQLServer } = require('parse-server');
const app = express();

const parseServer = new ParseServer({
  databaseURI: 'mongodb://localhost:27017/test',
  appId: 'APPLICATION_ID',
  masterKey: 'MASTER_KEY',
  serverURL: 'http://localhost:1337/parse',
  publicServerURL: 'http://localhost:1337/parse',
  // liveQuery permet d'avoir la synchronisation des données
  // https://docs.parseplatform.org/parse-server/guide/#live-queries
  liveQuery: {
    classNames: ['BetFormated']
  }
});


var port = 1337;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function() {
    console.log('API running on http://localhost:1337/parse');
});
var parseLiveQueryServer = ParseServer.createLiveQueryServer(httpServer);
app.use('/parse', parseServer.app); 

```

 * Démarrer MongoDB localement dans un dossier ```data```

```sh
mongod --dbpath data&
```
ou de façon global avec
```sh
systemctl start mongod
```
 * Démarrer Parse Server avec 
```sh
node index.js
```
ou installer [pm2] et démarrer avec
```sh
pm2 start index.js
```



## Utilisation
Lancer le script main.py
```sh
python3 main.py
```

Le script doit être lancé régulièrement, toutes les minutes par exemple, afin d'enregistrer l'évolution des cotes et des rapports.

On peut utiliser une tâche Cron pour cela.

Exemple pour lancer le script toutes les minutes entre 5h et 22h. Le script se trouve dans le dossier ```/dev/aspibet_scraper/```

```
* 5-22 * * *  cd /dev/aspibet_scraper/ && /usr/bin/python3 /dev/aspibet_scraper/main.py > /dev/null 2>&1
```



## Export de la base MongoDB en csv
 * Exemple si vous souhaitez faire un export en csv
```sh
mongoexport --host localhost --db test --collection Bet --type=csv --out text.csv --fields jour,reun,prix,num,rapportDirectInline,rapportDirectOffline,dateRapport,enjeuInline,totalEnjeuInline,updateTimeInline,enjeuOffline,totalEnjeuCheval1,totalEnjeuOffline,updateTimeOffline
```


# Remerciements

Si vous souhaitez remercier l'auteur, faite un don pour un projet libre tel que [VLC] ou [Wikimedia]

# Licence GPLv3

 > This file is part of Aspibet.

 > Aspibet is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

 > Aspibet is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

 > You should have received a copy of the GNU General Public License along with Aspibet.  If not, see <https://www.gnu.org/licenses/>.

[VLC]: <https://www.videolan.org/contribute.html#money>
[Wikimedia]: <https://dons.wikimedia.fr/souteneznous/1~mon-don>
[Parse Server]: <https://github.com/parse-community/parse-server#readme>
[pm2]: <https://pm2.keymetrics.io>

